# Code Formatting Rulesets

A collection of code formatting rulesets providing a jumpstart for open source contributions.

Most open source projects have defined a set of specific code formatting rules, or even check for respecting them automatically when sending a pull request with a code contribution. For example, Apache Maven plugins are even checked for whitespace changes. Unfortunately not all open source projects publish a machine-readable ruleset, or even worse, some do not even publish a human-readable ruleset.

Within this project I publish the rulesets I have set up for myself after decades of open source contribution. Feel free to send a PR if you think there is a bug or in case you have a new rulset set up for an open source project. I will be happy to extend my collection.